package cn.jingchengyuanda.model;

import cn.jingchengyuanda.entity.PageBean;

/**
 * -带分页的基础model
 * @author Dictate
 *
 */
public abstract class BasePageModel extends BaseModel{

  private static final long serialVersionUID = 4651325731919604800L;
  private PageBean page =new PageBean();
   public BasePageModel() {
    
  }
  public PageBean getPage() {
    return page;
  }
  public void setPage(PageBean page) {
    this.page = page;
  }
   
}
