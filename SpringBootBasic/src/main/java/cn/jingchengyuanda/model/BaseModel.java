package cn.jingchengyuanda.model;

import cn.jingchengyuanda.entity.BaseEntity;
import cn.jingchengyuanda.entity.TbToken;

/**
 * model层（获取客户端用户数据） 基础类
 * @author Dictate
 *
 */
public abstract class BaseModel extends BaseEntity {

  private static final long serialVersionUID = -1229289419639870785L;
  /**
   * token -客户端标识，由服务器端管理，客户端每次都需要提交数据
   */
  private String token;
  
  public BaseModel() {
    
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
  
  /**
   * getToken的委托方法，将客户端获取的token信息传递给TbToken对象
   * @return
   */
  public TbToken makeTokenInfo() {
    TbToken tbToken =new TbToken();
    tbToken.setToken(token);
    return tbToken;
  }
  
  

}
