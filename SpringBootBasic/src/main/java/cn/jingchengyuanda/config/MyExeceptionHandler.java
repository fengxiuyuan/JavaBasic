package cn.jingchengyuanda.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.jingchengyuanda.entity.JsonMessage;
import cn.jingchengyuanda.exception.AppException;
/**
 * MyExeceptionHandler全局错误处理器
 * Controller层全局错误处理器
 * ControllerAdvice注解表示控制器层拦截处理（aop-面向切面编程）
 * ExceptionHandler注解表示方法为错误处理器，参数是错误类型
 * @author Dictate
 *
 */
@ControllerAdvice
@ResponseBody
public class MyExeceptionHandler {
  
  private static final Logger log = LoggerFactory.getLogger(MyExeceptionHandler.class);
  
  @ExceptionHandler(Exception.class)
  public JsonMessage hanleException(Exception ex) {
    log.error("服务器发生错误:",ex);
    //处理应用程序自定义异常
    if (ex instanceof AppException) {
      AppException appException=(AppException) ex;
      return JsonMessage.getFail(appException.getCode(),appException.getMessage());
      
    }
    return JsonMessage.getFail("服务器忙，请稍后重试");
  }

}
