package cn.jingchengyuanda.config;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
/**
 * mvc配置
 * @author Dictate
 *
 */
@Configuration
public class MywebConfig implements WebMvcConfigurer {
  @Override
  public void addCorsMappings(CorsRegistry registry) {
    //跨域配置
    WebMvcConfigurer.super.addCorsMappings(registry);
    //addMapping表示添加跨越的url映射，/**表示所有的页面都支持跨域
    //   /**表示所有的页面都支持跨域，一般会将业务处理的url
    //allowedMethods表示允许的http事件（post ，get，DELETE.....）
    //allowedOrigins表示允许跨域的域名，生产环境中一定会配置成限定的域名
    //比如前端域名是huhuiyu.top,而springboot后端域名为api，huhuiyu.top
    //那么因该允许的域名因该配置成huhuiyu.top,或者相对危险的*.huhuiyu.top
    //allowCredentials表示是否需要开启认证
    registry.addMapping("/**").allowedMethods("*").allowedOrigins("*").allowCredentials(false);
  }
    
     @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
      
      WebMvcConfigurer.super.configureMessageConverters(converters);
      FastJsonHttpMessageConverter converter =new FastJsonHttpMessageConverter();
      //这里需要配置json转换细节
      converters.add(0,converter);
    }
}
