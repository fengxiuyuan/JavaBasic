package cn.jingchengyuanda.converter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cn.jingchengyuanda.utils.MyUtils;


/**
 * DateConverter-日期数据转换器
 * 
 * @author Dictate
 *
 */
@Component
public class DateConverter implements Converter<String, Date> {
  /**
   * 短格式日期
   */
  private static final SimpleDateFormat SDF_SHORT = new SimpleDateFormat("yyyy-MM-dd");
  /**
   * 长格式日期
   */
  private static final SimpleDateFormat SDF_LONG = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  private static final Pattern PSHORT = Pattern.compile("^\\d{4}[-]\\d{2}[-]\\d{2}$");

  private static final Pattern PLONG = Pattern.compile("^\\d{4}[-]\\d{2}[-]\\d{2} \\d{2}:\\d{2}:\\d{2}$");

  @Override
  public Date convert(String source) {
    if (MyUtils.isEmpty(source)) {
      return null;
    }
    source=MyUtils.trim(source);
    try {
      if (PLONG.matcher(source).matches()) {
        return SDF_LONG.parse(source);
      } else if (PSHORT.matcher(source).matches()) {
        return SDF_SHORT.parse(source);
      }
    } catch (Exception e) {
      // TODO: handle exception
    }
    return null;
  }

  public static void main(String[] args) {
    // 正则表达式测试 \d是数字，{n,m}是量词，[指定的字符列表] \d也可以是[0123456789]^是行开头，$是行结尾
    // 四位数-两位数-两位数
    // 如果校验座机电话：[0]\\d{2,3}[-]\\d{7,8}
    //    Pattern pattern = Pattern.compile("^\\d{4}[-]\\d{2}[-]\\d{2}$");    //    String info = "44123-88-77";
   //    System.out.println(pattern.matcher(info).matches());
  //    info = "2340-88-77 12:34:69";
  //    System.out.println(pattern.matcher(info).matches());
  
  }

}
