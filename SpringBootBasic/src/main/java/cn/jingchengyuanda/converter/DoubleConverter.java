package cn.jingchengyuanda.converter;

import org.mockito.internal.util.collections.ListUtil.Converter;
import org.springframework.stereotype.Component;

import cn.jingchengyuanda.utils.MyUtils;

/**
 * Double数值转换
 * @author Dictate
 *
 */
@Component
public class DoubleConverter implements Converter<String,Double>{
  @Override
  public Double convert(String source) {
    if (MyUtils.isEmpty(source)) {
      return null;
    }
    source=MyUtils.trim(source);
    try {
      return Double.valueOf(source);
    } catch (Exception ex) {
      
    }
    return null;
  }
  
}
  


