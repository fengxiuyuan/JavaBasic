package cn.jingchengyuanda.converter;

import org.mockito.internal.util.collections.ListUtil.Converter;
import org.springframework.stereotype.Component;

import cn.jingchengyuanda.utils.MyUtils;

/**
 * Integer数值转换器
 * @author Dictate
 *
 */
@Component
public class IntegerConverter implements Converter<String, Integer> {
 @Override
  public Integer convert(String source) {
    if (MyUtils.isEmpty(source)) {
      return null;
    }
    source=MyUtils.trim(source);
    try {
      return Integer.parseInt(source);
    } catch (Exception ex) {
      
    }
    return null;
  }
}
