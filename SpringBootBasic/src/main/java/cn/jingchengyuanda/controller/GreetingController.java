package cn.jingchengyuanda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.jingchengyuanda.config.MyConfig;
import cn.jingchengyuanda.entity.Greeting;
import cn.jingchengyuanda.entity.JsonMessage;
/***
 *  
 * @author Dictate
 *
 */
@RestController
@ResponseBody
@RequestMapping("/greeting")
public class GreetingController {

  @Autowired
  private MyConfig myConfig;
  
  @RequestMapping(name="")
  public JsonMessage index() {
  //http://127.0.0.1:20000
    String m =String.format("欢迎使用springboot, %s,%s,%s",myConfig.appName,myConfig.reload,myConfig.age);
    JsonMessage message =JsonMessage.getSuccess(m);
    return message;
  }
  
  @RequestMapping("/greeting")
  public JsonMessage greeting(Greeting greeting) {
    //http://127.0.0.1:20000/greeting
    //http://127.0.0.1:20000/greeting?id=100
    //http://127.0.0.1:20000/greeting?id=101&content=abc123
    JsonMessage message =JsonMessage.getSuccess("");
    message.getDatas().put("greeting", greeting);
    return message;
  }

}
