package cn.jingchengyuanda.controller.test;

import cn.jingchengyuanda.model.BaseModel;
import cn.jingchengyuanda.model.BasePageModel;
/**
 * TestModel -测试用的model层，model层可以组合不同
 * @author Dictate
 *
 */
public class TestModel extends BasePageModel {

  private static final long serialVersionUID = 1355780857633550560L;
  /**
   * model层定义的对象数据，可以直接new创建出来，可以减少null值判断
   */
  private TestEntity entity=new TestEntity();
  
  public TestModel() {
    
  }
  public TestEntity getEntity() {
    return entity;
  }
  public void setEntity(TestEntity entity) {
    this.entity = entity;
  }
 
}
