package cn.jingchengyuanda.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.jingchengyuanda.entity.TbToken;

/**
 * TestDAO-测试用的mapper
 * @author Dictate
 *
 */
@Mapper
public interface TestDAO {
  /**
   * Tbtoken的分页查询
   * @return
   * @throws Exception
   */
  List<TbToken>queryTokens() throws Exception;

}
