package cn.jingchengyuanda.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.jingchengyuanda.entity.TbUser;

/**
 * TbUser的DAO
 * 
 * @author Dictate
 *
 */
@Mapper
public interface TbUserDAO {
  /**
   * login 是用户登录信息校验
   * 
   * @param user 登录用户信息
   * @return 正确就是返回用户完整信息，否则返回null
   * @throws Exception
   */
  TbUser login(TbUser user) throws Exception;

}
