package cn.jingchengyuanda.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Mapper;
/**
 * Utils-系统工具
 * @author Dictate
 *
 */
@Mapper
public interface UtilsDAO {
  /**
   * 查询系统事件
   * 
   * @return
   */
  Date queryTime();
  

}
