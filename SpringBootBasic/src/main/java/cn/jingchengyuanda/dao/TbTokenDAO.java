package cn.jingchengyuanda.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.jingchengyuanda.entity.TbToken;

/**
 * Token追踪
 * 如果客户端没有提供token，创建新的Token添加到数据库中，返回token给客户端
 * 如果客户端提供了token，就判断token是否数据库中存在且没有过期（lastupdate）
 * 不存在或者过期就回到新的流程
 * 存在就更新过期时间（lasyupupdate）
 * 如果token对应的客户端长期不向服务器发起数据，会造成数据库资源浪费
 * 所以要开启定时任务检查token的lastupdate，超过一定时间就会自动清除4
 * @author Dictate
 *
 */
@Mapper
public interface TbTokenDAO {
  /**
   * 添加token
   * @param token
   * @return
   */
  int addToken(TbToken token);
  /**
   * 更新token时间
   * @param token
   * @return
   */
 int updateToken(TbToken token);
 
 /**
  * 查询token是否存在
  * @param token
  * @return
  */
 TbToken queryTbToken(TbToken token);
 
 /**
  * deleteTokens -过期的token
  * @return
  * @throws Exception
  */
 int deleteTokens() throws Exception;

}
