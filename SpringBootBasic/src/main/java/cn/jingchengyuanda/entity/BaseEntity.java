package cn.jingchengyuanda.entity;

import java.io.Serializable;

import com.alibaba.fastjson.JSONObject;
/**
 * Entity和数据传输类型的对象的基础类，统一ToString为JSon格式
 * @author Dictate
 *
 */
public class BaseEntity implements Serializable {

  private static final long serialVersionUID = -486560487357717126L;
  
  @Override
    public String toString() {
    //创建fastjson的json对象
    JSONObject json=new JSONObject();
    //将当前实例以当前类的名称为key放入json对象中
    json.put(this.getClass().getName(), this);
    //返回json格式字符串
      return json.toString();
    }

}
