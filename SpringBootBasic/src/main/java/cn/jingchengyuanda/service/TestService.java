package cn.jingchengyuanda.service;

import cn.jingchengyuanda.entity.JsonMessage;
import cn.jingchengyuanda.entity.PageBean;

/**
 * TestService-测试用服务
 * @author Dictate
 *
 */
public interface TestService {

  /**
   * 1-分页查询
   * @param page
   * @return
   * @throws Exception
   */
  JsonMessage queryTokens(PageBean page) throws Exception;

}
