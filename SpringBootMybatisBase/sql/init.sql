/*数据初始化脚本*/
use SpringBootMybatisBase;
truncate table TbConfig;
truncate table TbUser;
truncate table TbToken;
truncate table TbTokenInfo;
/*系统配置数据*/
/*token过期时间配置，值是分钟数*/
insert into TbConfig(configKey,configValue,lastuodate)values('token.timeout','30',now());

/*默认用户测试*/
insert into TbUser(username,password,nickname)values('test','test-pwd','测试用户表');

/*查询*/
select configKey,configValue,lastuodate from TbConfig;
select uid,username,password,nickname,isEnable,regDate from TbUser;
select token,uid,lastupdate from TbToken;
select token,infoKey,info,lastupdate from TbTokenInfo;
