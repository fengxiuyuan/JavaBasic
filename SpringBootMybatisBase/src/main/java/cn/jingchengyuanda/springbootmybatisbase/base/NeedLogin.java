package cn.jingchengyuanda.springbootmybatisbase.base;

import cn.jingchengyuanda.springbootmybatisbase.entiy.TbUser;

/**
 * 需要登入的标准接口
 * @author Dictate
 *
 */
public interface NeedLogin {
  /**
   * 没有登录的错误代码
   */
  public static final int NOT_LOGIN=1000;
  /**
   * 传入登录的用户信息
   * @param user
   */
  public void setUser(TbUser user);
  
  

}
