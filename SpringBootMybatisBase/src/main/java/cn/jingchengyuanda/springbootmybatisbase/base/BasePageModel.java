package cn.jingchengyuanda.springbootmybatisbase.base;

import cn.jingchengyuanda.springbootmybatisbase.utils.PageBean;

/**
 * -带分页信息的model层
 * @author Dictate
 *
 */
public class BasePageModel  extends BaseEntity{

  private static final long serialVersionUID = -6480734765189170863L;
  private PageBean page =new PageBean();
  public BasePageModel() {
    
  }
  public PageBean getPage() {
    return page;
  }
  public void setPage(PageBean page) {
    this.page = page;
  }
  
}
