package cn.jingchengyuanda.springbootmybatisbase.aop;

import org.aspectj.lang.annotation.Pointcut;

/**
 * -应用程序切点定义
 * 
 * @author Dictate
 *
 */
public class AppPointCut {
  @Pointcut("execution(public * cn.jingchengyuanda.springbootmybatisbase.controller..*.*(..))")
  public void controller() {
  }

}
