package cn.jingchengyuanda.springbootmybatisbase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jingchengyuanda.springbootmybatisbase.model.IndexModel;
import cn.jingchengyuanda.springbootmybatisbase.service.IndexService;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * -首页
 * 
 * @author Dictate
 *
 */
@RestController
public class IndexController {
  @Autowired
  private IndexService indexService;

  @RequestMapping("")
  public JsonMessage index(IndexModel model) throws Exception {
    // 127.0.0.1:20000?echo=123
    return indexService.index(model);
  }

}
