package cn.jingchengyuanda.springbootmybatisbase.model;

import cn.jingchengyuanda.springbootmybatisbase.base.BaseModel;

/**
 * 首页model层
 * 
 * @author Dictate
 *
 */
public class IndexModel extends BaseModel {

  private static final long serialVersionUID = -5749450205561785040L;
  private String echo;

  public IndexModel() {

  }

  public String getEcho() {
    return echo;
  }

  public void setEcho(String echo) {
    this.echo = echo;
  }

}
