package cn.jingchengyuanda.springbootmybatisbase.service;

import cn.jingchengyuanda.springbootmybatisbase.model.IndexModel;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * 服务首页
 * @author Dictate
 *
 */
public interface  IndexService {
  
  /**
   * -首页echo
   * @param model
   * @return
   * @throws Exception
   */
  JsonMessage index(IndexModel model) throws Exception;
  
  
  
  
  

}
