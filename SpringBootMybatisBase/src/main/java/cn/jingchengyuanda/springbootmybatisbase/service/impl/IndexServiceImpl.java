package cn.jingchengyuanda.springbootmybatisbase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.jingchengyuanda.springbootmybatisbase.dao.UtilsDAO;
import cn.jingchengyuanda.springbootmybatisbase.model.IndexModel;
import cn.jingchengyuanda.springbootmybatisbase.service.IndexService;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * 
 * @author Dictate
 *
 */
@Service
@Transactional(rollbackFor =Exception.class)
public class IndexServiceImpl implements IndexService {
  @Autowired
  private UtilsDAO utilsDAO;
  
  @Override
  public JsonMessage index(IndexModel model)throws Exception{
    JsonMessage message =JsonMessage.getSuccess(model.getEcho());
    message.getDatas().put("now",utilsDAO.queryTime());
    return message;
  }
}
