package cn.jingchengyuanda.springbootmybatisbase.service;

import cn.jingchengyuanda.springbootmybatisbase.entiy.TbTokenInfo;
import cn.jingchengyuanda.springbootmybatisbase.model.UtilModel;

/**
 * 
 * @author Dictate
 *
 */
public interface UtilService {
 /**
  * -生产图片校验码
  * @param model
  * @return
  * @throws Exception
  */
  String makeImageCode(UtilModel model) throws Exception;
/**
 * 校验图片验证码是否正确，服务器端的图片校验码只能使用一次
 * @param tokenInfo
 * @return
 * @throws Exception
 */
boolean checkImageCode(TbTokenInfo tokenInfo) throws Exception;

  
}
