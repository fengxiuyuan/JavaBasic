package cn.jingchengyuanda.springbootmybatisbase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.jingchengyuanda.springbootmybatisbase.dao.TbTokenInfoDAO;
import cn.jingchengyuanda.springbootmybatisbase.entiy.TbTokenInfo;
import cn.jingchengyuanda.springbootmybatisbase.model.UtilModel;
import cn.jingchengyuanda.springbootmybatisbase.service.UtilService;
import cn.jingchengyuanda.springbootmybatisbase.utils.ImageCode;
import cn.jingchengyuanda.springbootmybatisbase.utils.MyUtils;

/**
 * 
 * @author Dictate
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UtilServiceImpl implements UtilService {
  @Autowired
  private TbTokenInfoDAO tbTokenInfoDAO;
  
  @Override
  public String makeImageCode(UtilModel model)throws Exception{
    //产生图片校验码
    String code =ImageCode.makeCode();
    //获取token信息
    TbTokenInfo tokenInfo =model.makeTbTokenInfo();
    //判断图片校验码是否存在
    TbTokenInfo sinfo=tbTokenInfoDAO.queryImageCode(tokenInfo);
    if (sinfo==null) {
      //不存在就将图片code写入
      tokenInfo.setInfo(code);
      tbTokenInfoDAO.addImageCode(tokenInfo);
    }else {
      //存在就更新图片code
      sinfo.setInfo(code);
      tbTokenInfoDAO.updateImageCode(sinfo);
    }
     return code;
  }
  @Override
  public boolean checkImageCode(TbTokenInfo tokenInfo)throws Exception{
    //code不存在就返回false
    if (MyUtils.isEmpty(tokenInfo.getInfo())) {
      return false;
    }
    //获取数据库中的code
    TbTokenInfo sinfo=tbTokenInfoDAO.queryImageCode(tokenInfo);
    if (sinfo==null) {
      //不存在就返回fasle
      return false;
      
    }
    //删除数据库中的code(只能使用一次)
    tbTokenInfoDAO.deleteImageCode(sinfo);
    //返回数据中code和传入的code对比结果
    return sinfo.getInfo().equalsIgnoreCase(tokenInfo.getInfo());
  }
}
