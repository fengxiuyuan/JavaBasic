package cn.jingchengyuanda.springbootmybatisbase.service;

import cn.jingchengyuanda.springbootmybatisbase.model.TestModel;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * 测试服务
 * @author Dictate
 *
 */
public interface TestService {
  /**
   * 测试事务添加
   * 
   * @param model
   * @return
   * @throws Exception
   */
  JsonMessage TranAdd(TestModel model) throws Exception;

}
