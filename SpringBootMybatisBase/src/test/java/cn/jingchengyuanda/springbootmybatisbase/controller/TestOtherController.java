package cn.jingchengyuanda.springbootmybatisbase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jingchengyuanda.springbootmybatisbase.entiy.TbTokenInfo;
import cn.jingchengyuanda.springbootmybatisbase.model.TestModel;
import cn.jingchengyuanda.springbootmybatisbase.service.TestService;
import cn.jingchengyuanda.springbootmybatisbase.service.UtilService;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * 
 * @author Dictate
 *
 */
@RestController
@RequestMapping("/test")
public class TestOtherController {
  @Autowired
  private TestService testService;
  @Autowired
  private UtilService utilService;

  /**
   * 测试事务
   * 
   * @param model
   * @return
   * @throws Exception
   */
  @RequestMapping("/tran")
  public JsonMessage tran(TestModel model) throws Exception {
    // 127.0.0.1:20000/test/tran?token=1fe3ce5a-7379-4c98-aaf1-2a8f6271bde8&ctoken.token=123&tokenInfo.infoKey=123&tokenInfo.info=qqwe
    model.getTokenInfo().setToken(model.getCtoken().getToken());
    return testService.TranAdd(model);
  }
  
  @RequestMapping("/imageCode")
  public JsonMessage imageCode(TestModel model)throws Exception{
    //127.0.0.1:20000/test/imageCode?token=&imageCode=
    //获取图片校验码
    TbTokenInfo info =model.makeTbTokenInfo();
    info.setInfo(model.getImageCode());
    //校验
    boolean check =utilService.checkImageCode(info);
    if (check) {
      return JsonMessage.getSuccess("图片校验正确");
    }else {
      return JsonMessage.getFail("图片校验失败");
    }
  }

}