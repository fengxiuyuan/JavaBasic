package cn.jingchengyuanda.springbootmybatisbase.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.jingchengyuanda.springbootmybatisbase.entiy.TbToken;
import cn.jingchengyuanda.springbootmybatisbase.entiy.TbTokenInfo;

@Mapper
public interface TestDAO {
  /**
   * 添加tokeninfo
   * 
   * @param tokenInfo
   * @return
   * @throws Exception
   */
  int addTokenInfo(TbTokenInfo tokenInfo) throws Exception;

  /**
   * 添加token
   * 
   * @param token
   * @return
   * @throws Exception
   */
  int addToken(TbToken token) throws Exception;

}
