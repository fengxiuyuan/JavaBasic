package cn.jingchengyuanda.demo;

import java.io.Serializable;

/**
 * 演示fastjson的实体类
 * 
 * @author Dictate
 *
 */
public class FastJsonEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  private Integer eid;
  public String info;

  public FastJsonEntity() {

  }

  public Integer getEid() {
    return eid;
  }

  public void setEid(Integer eid) {
    this.eid = eid;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  @Override
  public String toString() {
    return "FastJsonEntity [eid=" + eid + ", info=" + info + "]";
  }

}
