package cn.jingchengyuanda.springbootmybatisbase.controller;

import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jingchengyuanda.springbootmybatisbase.model.UtilModel;
import cn.jingchengyuanda.springbootmybatisbase.service.UtilService;
import cn.jingchengyuanda.springbootmybatisbase.utils.ImageCode;

/**
 * 工具类控制器
 * 
 * @author Dictate
 *
 */

@RestController
@RequestMapping("/util")
public class UtilController {

  @Autowired
  private UtilService utilService;

  @RequestMapping("/validate.jpg")
  public void imageCode(UtilModel model, HttpServletResponse response) throws Exception {
    
    //127.0.0.1:20000/util/validate.jpg?token=4a3d7e74-dcc8-4e8e-a283-89b3cd285d3e
    // 自定义应答类型为图片
    response.setContentType("image/jpeg");
    // 自定义应答为图片
    String code = utilService.makeImageCode(model);
    // 将图片通过respone输出
    OutputStream os = response.getOutputStream();
    ImageIO.write(ImageCode.makeImage(code), "jpeg", os);
    os.close();
  }
}
