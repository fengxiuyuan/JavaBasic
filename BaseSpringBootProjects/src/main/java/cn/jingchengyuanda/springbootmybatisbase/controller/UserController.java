package cn.jingchengyuanda.springbootmybatisbase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jingchengyuanda.springbootmybatisbase.service.UserService;
import cn.jingchengyuanda.springbootmybatisbase.service.impl.UserModel;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * -用户信息控制器
 * @author Dictate
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {
  @Autowired
  private UserService userService;
  
  @RequestMapping("/login")
  public JsonMessage login(UserModel model)throws Exception{
    return userService.login(model);
  }
  
  @RequestMapping("/logout")
  public JsonMessage logout(UserModel model)throws Exception{
    return userService.logout(model);
  }
  
  //http://127.0.0.1:20000/user/login?token=e6fa719f-35d4-46a0-b00f-6af021410997&user.username=test&user.password=test-pwd
  @RequestMapping("/getUserInfo")
  public JsonMessage getUserInfo(UserModel model)throws Exception{
    return userService.getUserInfo(model);
  }
}

