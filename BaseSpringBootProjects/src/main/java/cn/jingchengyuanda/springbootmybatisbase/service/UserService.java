package cn.jingchengyuanda.springbootmybatisbase.service;

import cn.jingchengyuanda.springbootmybatisbase.service.impl.UserModel;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * user控制器的服务
 * @author Dictate
 *
 */
public interface UserService {
  /**
   * -用户登录
   * @param model
   * @return
   * @throws Exception
   */
  JsonMessage login(UserModel model) throws Exception;
  /**
   * -用户注销
   * @param model
   * @return
   * @throws Exception
   */
  JsonMessage logout(UserModel model) throws Exception;
  /**
   * -获取token中的信息
   * @param model
   * @return
   * @throws Exception
   */
  JsonMessage getUserInfo(UserModel model) throws Exception;

}
