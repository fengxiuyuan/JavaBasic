package cn.jingchengyuanda.springbootmybatisbase.service.impl;

import cn.jingchengyuanda.springbootmybatisbase.base.BaseModel;
import cn.jingchengyuanda.springbootmybatisbase.entiy.TbUser;

/**
 * -User控制器的模型
 * 
 * @author Dictate
 *
 */
public class UserModel extends BaseModel {

  private static final long serialVersionUID = 2874451800374094262L;
  private TbUser user = new TbUser();

  public UserModel() {

  }

  public TbUser getUser() {
    return user;
  }

  public void setUser(TbUser user) {
    this.user = user;
  }

}
