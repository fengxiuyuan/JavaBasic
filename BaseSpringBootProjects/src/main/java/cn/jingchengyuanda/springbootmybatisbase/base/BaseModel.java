package cn.jingchengyuanda.springbootmybatisbase.base;
/**
 * -model层基类
 * @author Dictate
 *
 */

import cn.jingchengyuanda.springbootmybatisbase.entiy.TbToken;
import cn.jingchengyuanda.springbootmybatisbase.entiy.TbTokenInfo;

public abstract class BaseModel extends BaseEntity {

  private static final long serialVersionUID = 4488899551297760013L;
  private String token;

  public BaseModel() {

  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public TbToken makeTbToken() {
    TbToken tbToken = new TbToken();
    tbToken.setToken(token);
    return tbToken;
  }
 /**
  * -获取tokeninfo的委托方法
  * @return
  */
  public TbTokenInfo makeTbTokenInfo() {
    TbTokenInfo tokenInfo = new TbTokenInfo();
    tokenInfo.setToken(token);
    return tokenInfo;
  }
}
