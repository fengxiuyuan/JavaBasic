package cn.jingchengyuanda.springbootmybatisbase.base;

import java.io.Serializable;

import com.alibaba.fastjson.JSONObject;

/**
 * entity和数据传输类型的对象的基础类，统一tostring为json格式
 * 
 * @author Dictate
 *
 */
public abstract class BaseEntity implements Serializable {

  private static final long serialVersionUID = -1983749774073434851L;

  @Override
  public String toString() {
    // 创建fastjson的json对象
    JSONObject json = new JSONObject();
    // 将当前实列以当前类的名称为key放入json对象中
    json.put(this.getClass().getName(), this);
    // 返回json格式字符串
    return json.toString();
  }
}
