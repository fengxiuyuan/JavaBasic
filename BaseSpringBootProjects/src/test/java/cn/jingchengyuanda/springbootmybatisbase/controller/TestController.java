package cn.jingchengyuanda.springbootmybatisbase.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jingchengyuanda.springbootmybatisbase.base.NeedLogin;
import cn.jingchengyuanda.springbootmybatisbase.entiy.TbUser;
import cn.jingchengyuanda.springbootmybatisbase.model.TestModel;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * -测试控制器,实现了needLoin接口，会被注入登录的user信息，否则该控制器无法进入
 * 
 * @author Dictate
 *
 */
@RestController
@RequestMapping("/test")
public class TestController implements NeedLogin {
  
  private TbUser user;

  @Override
  public void setUser(TbUser user) {
    this.user = user;
  }
  
  
 /**
  * 测试NeedLoin接口
  * @param model
  * @return
  * @throws Exception
  */
  @RequestMapping("/index")
  public JsonMessage index(TestModel model) throws Exception {
    //127.0.0.1:20000/test/index?token=1fe3ce5a-7379-4c98-aaf1-2a8f6271bde8
    JsonMessage message = JsonMessage.getSuccess("测试NeesLogin接口");
    // 可以直接拿到登录的user信息
    message.getDatas().put("user", user);
    return message;
  }
 
}
