package cn.jingchengyuanda.springbootmybatisbase.model;

import cn.jingchengyuanda.springbootmybatisbase.base.BaseModel;
/**
 * 测试用的model
 * @author Dictate
 *
 */
import cn.jingchengyuanda.springbootmybatisbase.entiy.TbToken;
import cn.jingchengyuanda.springbootmybatisbase.entiy.TbTokenInfo;

public class TestModel extends BaseModel {

  private static final long serialVersionUID = -7640586990093199857L;

  private TbToken ctoken = new TbToken();

  private TbTokenInfo tokenInfo = new TbTokenInfo();
  
  private String imageCode ="";

  public String getImageCode() {
    return imageCode;
  }

  public void setImageCode(String imageCode) {
    this.imageCode = imageCode;
  }

  public TbToken getCtoken() {
    return ctoken;
  }

  public void setCtoken(TbToken ctoken) {
    this.ctoken = ctoken;
  }

  public TbTokenInfo getTokenInfo() {
    return tokenInfo;
  }

  public void setTokenInfo(TbTokenInfo tokenInfo) {
    this.tokenInfo = tokenInfo;
  }

}
