package cn.jingchengyuanda.springbootmybatisbase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.jingchengyuanda.springbootmybatisbase.dao.TestDAO;
import cn.jingchengyuanda.springbootmybatisbase.model.TestModel;
import cn.jingchengyuanda.springbootmybatisbase.service.TestService;
import cn.jingchengyuanda.springbootmybatisbase.utils.JsonMessage;

/**
 * 
 * @author Dictate
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TestServiceImpl implements TestService {
  @Autowired
  private TestDAO testDAO;

  @Override
  public JsonMessage TranAdd(TestModel model) throws Exception {
    //事务处理，只有一个失败都会失败，否则都成功
    testDAO.addToken(model.getCtoken());
    testDAO.addTokenInfo(model.getTokenInfo());
    return JsonMessage.getSuccess("添加成功");
  }

}
