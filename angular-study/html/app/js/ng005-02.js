angular.element(document).ready(function() {
  var ctrls = angular.module('controllers');
  //多个控制器
  ctrls.controller('MyCtrl02', ['$scope', '$log', MyCtrl02]);

  function MyCtrl02($scope, $log) {
    $log.debug('in MyCtrl02 init..........');

    //处理scope销毁
    $scope.$on('$destroy', function() {
      $log.debug('MyCtrl02 destroy');
    });

    //业务逻辑
    $scope.welcome = '单选反选演示';

    //性别单选
    $scope.sex = 'm';
    //动态学历单选
    $scope.edus = [
      { eid: 100, name: '高中' },
      { eid: 101, name: '中专' },
      { eid: 102, name: '大专' },
      { eid: 103, name: '本科' }
    ];
    //默认选项的学历
    $scope.edu = $scope.edus[0].eid;

    $scope.changeEdu = function(eid) {
      $log.debug(eid);
      $scope.edu = eid;
    };
  }
});
