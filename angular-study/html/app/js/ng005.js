angular.element(document).ready(function() {
  var ctrls = angular.module('controllers');
  //多个控制器
  ctrls.controller('MyCtrl', [
    '$rootScope',
    '$scope',
    '$log',
    '$timeout',
    MyCtrl
  ]);

  function MyCtrl($rootScope, $scope, $log, $timeout) {
    $log.debug('in MyCtrl init..........');
    //$rootScope表示全局作用域，在所有的地方都可以使用，强烈不推荐使用
    $rootScope.appTitle = '我的angularjs学习';
    $rootScope.appTitle = '控制器里面定义的title';

    //checkbox的数据
    $scope.mycheck = true;
    $scope.cvalue = { value: 'abc', checked: false };
    

    $timeout(function() {
      $scope.cvalues[1].checked = true;
    }, 2000);

    $scope.selectedCv = function() {
      var info = [];
      for (var i = 0; i < $scope.cvalues.length; i++) {
        var cv = $scope.cvalues[i];
        if (cv.checked) {
          info.push(cv.value);
        }
      }
      return info.join(',');
    };

    //页面映射
    var includes ={
      inc01:'ng005-01.html',
      inc02:'ng005-02.html'
      };
      $scope.inc='';
      //切换包含页面
      $scope.changePage=function(page){
      $scope.inc =includes[page];
      };
  
      //默认页面
      $scope.changePage('inc01')
      
      $scope.cvalues = [
        { text: '显示文本', value: 'abc', checked: false },
        { text: '显123', value: '123', checked: false }
      ];
  }
});
