$(function() {
  var imgs = ['002.jpg', '003.jpg', '004.jpg', '005.jpg', '006.jpg', '007.jpg']; //（设定想要显示的图片）
  var i = 0;
  var head = document.getElementById('head'); //获取DIV对象
  // head.style.backgroundImage = 'url("../images/003.jpg")'; //设置图片的初始图片为该路径的图片
  head.style.backgroundSize = 'cover';
  function changeImage() {
    i++;
    i = i % imgs.length; // 超过2则取余数，保证循环在0、1、2之间
    head.style.background = 'url("../images/' + imgs[i] + '")';
  }
  setInterval(changeImage, 3000); //循环调用time1()函数，时间间隔为2000ms
  //setInterval()函数，按照指定的周期（按毫秒计）来调用函数或表达式
});
