$(function() {
  console.log('in login.js.....');

  $('#btnCancel').click(function() {
    resetForm();
  });

  /*重置表单 */

  function resetForm() {
    $('#txtUsername').val('');
    $('#txtPassWord').val('');
    $('#divError').html('');
    $('#txtUsername').focus();
  }
  //回车键
  $('#txtUsername').keyup(function(event) {
    console.log(event);
    if (event.keyCode == 13) {
      $('#btnLogin').trigger('click');
    }
  });


  //回车键
  $('#txtPassWord').keyup(function(event) {
    console.log(event);
    if (event.keyCode == 13) {
      $('#btnLogin').trigger('click');
    }
  });

  $('#btnLogin').click(function() {
    dataService.send(
      '/user/login',
      {
        'user.username': $('#txtUsername').val(),
        'user.password': $('#txtPassWord').val()
      },
      function(data) {
        $('#divError').html('');
        //服务器应答错误
        if (data.code == 500) {
          $('#divError').html(data.message);
        }
        //用户错误情况
        if (
          data.message == '用户名必须填写' ||
          data.message == '用户名不存在'
        ) {
          $('#txtUsername').focus();
          return;
        }
        if (data.message == '密码必须填写' || data.message == '密码错误') {
          $('#txtPassWord').focus();
          return;
        }
        //alert(data.message);
        location.href = 'index.html';
      }
    );
  });
});
